package com.example.fragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    Fragment fragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void btn_1(View view) {
        fragment = new BlankFragment2();
        FragmentManager fm  = getSupportFragmentManager();
        FragmentTransaction Ft = fm.beginTransaction();
        Ft.replace(R.id.fragment,fragment);
        Ft.commit();
    }
    public void btn_2(View view) {
        fragment = new BlankFragment();
        FragmentManager fm  = getSupportFragmentManager();
        FragmentTransaction Ft = fm.beginTransaction();
        Ft.replace(R.id.fragment,fragment);
        Ft.commit();
    }
}
